require_relative "../utilities/utilities"
require_relative "../exceptions/wrong_position_error"
require_relative "../exceptions/no_path_solution_error"

# a Class represemted a Map, 
# it contains method to generate a map's face, 
# and another method to manipulate object above them
class Map
	attr_reader :x_grid, :y_grid, :is_plain_map, :is_obstacle_map, :wall_sign, :path_sign, :paths, :walls
	
	def initialize(x_grid, y_grid, wall_sign = "#", path_sign = ".")
		@x_grid = x_grid
		@y_grid = y_grid
		@is_plain_map = false
		@is_obstacle_map = false
		@wall_sign = wall_sign
		@path_sign = path_sign
		@walls = []
		@paths = []
	end

	def generate_plain_map() # generate plain map, without obstacle
		@land = Array.new(@y_grid) {Array.new(@x_grid) {@path_sign}}
		@is_obstacle_map = false
		@is_plain_map = true
	end

	def generate_obstacle_map() # generate obstacle map, be confirmed all point in any path can meet to each other with certain route 
		def add_frontier(frontier, x, y, another_x, another_y)
			avaiable = false
			frontier.each() do |front|
				if front[0] == [y, x]
					avaiable = true
				end
			end
			frontier << [[y, x], [another_y, another_x]] if yield && @land[y][x] != @path_sign && !avaiable
		end

		@land = Array.new(@y_grid) {Array.new(@x_grid) {@wall_sign}}
		coords = Utilities.random_coordinate(0, 0, @x_grid-1, @y_grid-1)
		x, y = coords[0], coords[1]
		@land[y][x] = @path_sign
		@paths << [y, x]
		frontier = []
		loop do
			add_frontier(frontier, x, y+2, x, y+1) {y+2 < @y_grid}
			add_frontier(frontier, x+2, y, x+1, y) {x+2 < @x_grid}
			add_frontier(frontier, x, y-2, x, y-1) {y-2 > -1}
			add_frontier(frontier, x-2, y, x-1, y) {x-2 > -1}

			break if frontier.length() == 0
			new_spot = frontier.sample()
			@land[new_spot[0][0]][new_spot[0][1]] = @path_sign
			@land[new_spot[1][0]][new_spot[1][1]] = @path_sign
			@paths << new_spot[0] 
			@paths << new_spot[1]
			frontier.delete(new_spot)
			x, y = new_spot[0][1], new_spot[0][0]
		end
		for i in 0 ... @land.length()
			for j in 0 ... @land[i].length()
				if @land[i][j] == @wall_sign
					@walls << [i, j]
				end
			end
		end
		@is_plain_map = false
		@is_obstacle_map = true
	end

	def get_spot(x, y)
		if x < 0 || x >= @x_grid || y < 0 || y >= @y_grid
			raise(WrongPositionError, "position must be in range 0 <= x < #{@x_grid} and 0 <= y < #{@y_grid}")
		else
			return @land[y][x]
		end
	end

	def change_spot(x, y, sign) # change a point, with certain coordinate/position
		if x < 0 || x >= @x_grid || y < 0 || y >= @y_grid
			raise(WrongPositionError, "position must be in range 0 <= x < #{@x_grid} and 0 <= y < #{@y_grid}")
		else
			@land[y][x] = sign
			if sign == @wall_sign
				@walls << [y, x]
			else
				@paths << [y, x]
			end
		end
	end

	def change_path_sign(path_sign = ".") # change all sign in path with certain sign
		@path_sign = path_sign
		if @is_obstacle_map
			@paths.each() do |index|
				@land[index[0]][index[1]] = path_sign
			end
			return true
		else
			return false
		end
	end

	def change_wall_sign(wall_sign = "#") # change for every wall in walls with certain sign
		@wall_sign = wall_sign
		if @is_obstacle_map
			@walls.each() do |index|
				@land[index[0]][index[1]] = wall_sign
			end
			return true
		else
			return false
		end
	end
	
	def get_path(x_start, y_start, x_end, y_end)	#g et optinum path between two points and return 2D array contains all coordinates 
		def add_data(frontier, marked, spot_data, x_before, y_before, x_after, y_after, x_end, y_end)
			if yield && get_spot(x_after, y_after) == @path_sign && 
				!frontier.include?([y_after, x_after]) &&
				!marked.include?([y_after, x_after])
				frontier << [y_after, x_after]
				spot_data[y_after][x_after][0] = spot_data[y_before][x_before][0] + 1
				spot_data[y_after][x_after][1] = spot_data[y_after][x_after][0] + (y_end - y_after) ** 2 + (x_end - x_after) ** 2
				spot_data[y_after][x_after][2] = [y_before, x_before]
			end
		end
		new_x, new_y = x_start, y_start
		frontier = [[new_y, new_x]]
		marked = []
		spot_data = Array.new(@y_grid+1) {Array.new(@x_grid+1) {[0, 0, []]}}
		loop do
			if new_x == x_end && new_y == y_end
				path = [[new_y, new_x]]
				loop do
					if new_y == y_start && new_x == x_start
						return path
					end
					new_spot = spot_data[new_y][new_x][2] 
					path << new_spot
					new_x, new_y = new_spot[1], new_spot[0]
				end
			else
				add_data(frontier, marked, spot_data, new_x, new_y, new_x, new_y+1, x_end, y_end) {new_y + 1 < @y_grid}
				add_data(frontier, marked, spot_data, new_x, new_y, new_x+1, new_y, x_end, y_end) {new_x + 1 < @x_grid}
				add_data(frontier, marked, spot_data, new_x, new_y, new_x, new_y-1, x_end, y_end) {new_y - 1 > -1}
				add_data(frontier, marked, spot_data, new_x, new_y, new_x-1, new_y, x_end, y_end) {new_x - 1 > -1}
				frontier.delete([new_y, new_x])
				marked << [new_y, new_x]
				min = frontier[0]
				frontier.each() do |ind|
					if spot_data[ind[0]][ind[1]][0] < spot_data[min[0]][min[1]][0]
						min = [ind[0], ind[1]]
					end
				end
				if min.nil?
					raise(NoPathSolutionError, "There is no path solution!")
				end
				new_x, new_y = min[1], min[0]
			end
		end
	end
end