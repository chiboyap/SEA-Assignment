class Customer < User
	def initialize(name, age, id)
		super(name, age, id)
		@map_showable = true
		@orderable = true
		@history_viewable = true
	end
end