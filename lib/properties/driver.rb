class Driver < User
	def initialize(name, age, id)
		super(name, age, id)
		@map_showable = true
		@orderable = false
		@history_viewable = true
	end
end