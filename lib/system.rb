require_relative "utilities/utilities"
require_relative "properties/map"
require_relative "properties/user"
require_relative "properties/customer"
require_relative "properties/driver"


# a Class represented the system of go-cli
# it will use continuesly until the program is terminated by user
# the methods are used to manipulate whether object or interaction betwen system and user
class System
	PLAIN_MAP = "Ds21hz1aDgxaWadfzks581kSd012Sdf"
	OBSTACLE_MAP = "KSfh2a8oAsi2oap0sdfj21oUSDF2As"
	DEFAULT_USER_HISTORY_FILE_PATH = "../data/user history/--s--.txt"
	DEFAULT_USER_ACCOUNT_FILE_PATH = "../data/registered user/--s--.txt"

	attr_accessor :cost
	attr_reader :route_sign, :customer_sign, :driver_sign, :user_sign 
	
	def initialize(x_grid, y_grid, cost) #Constructor initialize map and cost for every path
		@users = {}
		@users_by_role = {customer: {}, driver: {}}
		@cache_information = {}
		@map = Map.new(x_grid, y_grid)
		@objects = Array.new(y_grid) {Array.new(x_grid) {[]}}
		@route_sign = "o"
		@user_sign = "U"
		@customer_sign = "C"
		@driver_sign = "D"
		@cost = cost
	end

	def generate_map(code) #generate map with 2 different face, plain or with obstacles
		if code == System::PLAIN_MAP
			@map.generate_plain_map()
		elsif code == System::OBSTACLE_MAP
			@map.generate_obstacle_map()
		end
	end

	def get_user(user_id) #find user from system
		return @users[user_id]
	end

	def move_user(user, x, y)	#move user from old position to new position
		for elem in @objects[user.position[0]][user.position[1]]
			if elem == user
				@objects[user.position[0]][user.position[1]].delete(user)
			end
		end
		@objects[y][x] << user
		user.set_position(x, y)
	end

	def add_user(user_name, role, age, x, y) #add new user, whether driver or customer
		if @map.get_spot(x, y) == @map.wall_sign
			@map.change_spot(x, y, @map.path_sign)
			loop do 
				coords = Utilities.random_coordinates([y+1, x], [y, x+1], [y-1, x], [y, x-1])
				if coords[0] > -1 && coords[0] < @map.y_grid && coords[1] > -1 && coords[1] < @map.x_grid
					@map.change_spot(coords[1], coords[0], @map.path_sign) 
					break
				end 
			end 
		end

		role_sym = role.downcase().to_sym()
		id = Utilities.hashing_id("ID", 10, @users_by_role.keys())
		@users_by_role[role_sym][id] = eval(role.capitalize()).new(Utilities.format_name(user_name), age, id)
		@users[id] = @users_by_role[role_sym][id]
		@users_by_role[role_sym][id].set_position(x, y)
		
		@objects[y][x] << @users[id]
		return id
	end

	def add_history(user_id, information_id) #add user's history
		user = get_user(user_id)
		if user.is_a?(Customer)
			history = get_information(information_id)
			if user.history == ""
				user.history += "\nYour trips: " + history
			else
				user.history += "\n" + history
			end
		elsif user.is_a?(Driver) 
		end
	end

	def get_history(user_id)	#get user's history
		user = get_user(user_id)
		if user.history_viewable
			return user.history
		end
	end

	def get_information(id) 	#get information and processing the information into text
		if id.start_with?("CH")
			info = @cache_information[id]
			info = "\nFrom (#{info[:customer].position[1]}, #{info[:customer].position[0]}) to "+
				"(#{info[:destination][0]}, #{info[:destination][1]})" +
				"\nTravel's distance\t: #{info[:distance]}" +
				"\nCost\t\t\t: #{info[:cost]}" +
				"\nDriver's name\t\t: #{info[:driver].get_name()}" +
				"\nDriver's position\t: (#{info[:driver].position[1]}, #{info[:driver].position[0]})"
		elsif id.start_with?("MAP")
			return @cache_information[id]
		end
	end

	def use_information(id)		#get information in certain format, and then delete the information from system
		info = @cache_information[id]
		@cache_information.delete(id)
		return info
	end

	def get_nearby_driver(x_start, y_start) # get nearby driver from one point
		distances = {}
		for driver in @users_by_role[:driver].values()
			actual_distance = @map.get_path(x_start, y_start, driver.position[1], driver.position[0])
			line_distance = (y_start - driver.position[0]) ** 2 + (x_start - driver.position[1]) ** 2
			distances[[actual_distance.length(), line_distance]] = [driver, actual_distance]
		end
		distances[distances.keys().sort![0]]
	end

	def order_go_ride(user_id, x_end, y_end) # get information to ordering go-ride, it contains route, nearby driver, and user information itself
		info = []
		user = get_user(user_id)
		if user.orderable
			driver_information = get_nearby_driver(user.position[1], user.position[0])
			path = @map.get_path(user.position[1], user.position[0], x_end, y_end)
			
			path.each() do |index|
				@map.change_spot(index[1], index[0], @route_sign)
			end
			
			map = get_map(user_id)
			
			path.each() do |index|
				@map.change_spot(index[1], index[0], @map.path_sign)
			end

			distance = path.length - 1
			cost = (distance) * @cost
			map_id = Utilities.hashing_id("MAP", 4, @cache_information.keys())
			@cache_information[map_id] = map
			history_id = Utilities.hashing_id("CH", 10, @cache_information.keys())
			@cache_information[history_id] = {customer: user, destination: [x_end, y_end] , 
				distance: distance, cost: cost, driver: driver_information[0], 
				driver_distance: driver_information[1]}
			return [map_id, history_id]
		else
			return [-1, -1]
		end
	end

	def go_ride(user_id, information_id)			# make an order go-ride comes true
		add_history(user_id, information_id)
		info = use_information(information_id)
		move_user(info[:customer], info[:destination][0], info[:destination][1])
		new_coordinate = Utilities.random_coordinate(0, 0, @map.x_grid-1, @map.y_grid-1)
		move_user(info[:driver], new_coordinate[1], new_coordinate[0])

		user_history = "#{info[:customer].position[1]}, #{info[:customer].position[0]}, " + 
			"#{info[:destination][0]}, #{info[:destination][1]}, #{info[:distance]}, #{info[:cost]}, " + 
			"#{info[:driver].get_name()}, #{info[:driver].position[1]}, #{info[:driver].position[0]}\n"
		file_name = "#{info[:customer].id.downcase()}"
		Utilities.create_file(user_history, 
			DEFAULT_USER_HISTORY_FILE_PATH.sub("--s--", file_name), "a")
		return "Adding history in file name: \"#{file_name}.txt\" (Without double quotes)"
	end

	def get_map_grid()  # getter method to get grid length for every dimension
		return [@map.x_grid, @map.y_grid]
	end

	def get_map(user_id) # get map more readable
		user = get_user(user_id)
		def get_spot(x, y)
			if @objects[y][x].length() == 0
				return(@map.get_spot(x, y))
			else
				customer_exist = false
				driver_exist = false
				@objects[y][x].each() do |user|
					if user.is_a?(Customer)
						customer_exist = true
					elsif user.is_a?(Driver)
						driver_exist = true
					end
					break if customer_exist && driver_exist
				end
				if customer_exist && driver_exist
					return(@user_sign)
				elsif customer_exist
					return(@customer_sign)
				else
					return(@driver_sign)
				end
			end
		end
		if user.map_showable()
			vertical_line = " "
			horizontal_line = " "
			left_gap = @map.y_grid.to_s.length + 3
			info = "\n" + "  ^".rjust(left_gap) + "\n" + 
					vertical_line.rjust(left_gap) + "\n"
			
			(@map.y_grid-1).downto(0) do |y|
				info += " " + y.to_s().rjust(@map.y_grid.to_s.length) + 
						" " + get_spot(0, y)
				1.upto(@map.x_grid-1) do |x|
					info += " "+ horizontal_line +" #{get_spot(x, y)}" 
				end

				if y != 0
					info += "\n" +vertical_line.rjust(left_gap)
					@map.x_grid.times() do
						info += "   " + vertical_line
					end
				else 
					info += " "+horizontal_line+" > "
				end
				info += "\n"
			end

			info += "0".rjust(left_gap) + "   "
			for number in 1 ... @map.x_grid do
				info += "#{number}".ljust(4)
			end
			info += "\n"
			return info + "\n\"#{$system.route_sign}\" = route sign, " + 
				"\"#{$system.customer_sign}\" = Customer (You), " + 
				"\"#{$system.driver_sign}\" = Drivers, " + 
				"\"#{$system.user_sign}\" = Drivers and Customer (You)\n" +
				"Also, you can see the map in [repo]/data/output/map.txt"
		else
			return " "
		end
	end
end