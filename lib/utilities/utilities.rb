# module contains all function can be used to help make another algorithm in codes
module Utilities
	def self.random_coordinate(x_start, y_start, x_end, y_end) #return one point 
		randomator = Random.new()
		x = randomator.rand(x_start .. x_end)
		y = randomator.rand(y_start .. y_end)
		return [y, x]
	end
	
	def self.random_coordinates(*args) #return one point from several points
		return args.sample()
	end

	def self.random_range(first_limit, second_limit)
		return Random.new().rand(first_limit .. second_limit)
	end

	def self.random_name_from_file(path) #to create random name for drivers
		randomator = Random.new()
		names = []
		File.open(path, "r") do |name_list|
			for name_in_line in name_list
				names << name_in_line[0 ... -1]
			end
		end
		return names.sample()
	end

	def self.simplify_name(name, length)
		simple_name = name
		name = name.split()
		(name.length()-1).downto(0) do |index|
			break if simple_name.length() <= length || name.length == 1
			simple_name = "#{name[0 ... index].join(" ")} #{(name[index .. -1].map() {|word| "#{word[0].capitalize()}."}).join(" ")}"
		end
		return simple_name
	end

	def self.format_name(user_name)
		user_name = user_name.downcase().split()
		perfect_user_name = user_name[0].capitalize()
		for index in 1 ... user_name.length()
			perfect_user_name += " " + user_name[index].capitalize()
		end
		return perfect_user_name
	end

	def self.hashing_id(prefix, length, data)
		randomator = Random.new()
		loop do
			id = prefix
			length.times() do 
				id += randomator.rand(0 .. 10).to_s()
			end
			if !data.include?(id)
				return id
			end
		end
	end

	def self.create_file(info, path, mode) #to create a file in particular path
		File.open(path, mode) do |file|
			file.write(info)
		end
	end 
end

