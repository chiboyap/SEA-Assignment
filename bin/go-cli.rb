#!/usr/bin/env ruby
require_relative "../lib/system"
require_relative "../lib/utilities/utilities"
require_relative "../lib/exceptions/wrong_position_error"
require_relative "../lib/exceptions/no_path_solution_error"


puts "Preparing..."
# Define variables
DEFAULT_INPUT_FILE_PATH = "../data/input/input.txt"
DEFAULT_MAP_FILE_PATH = "../data/output/map.txt"
DEFAULT_NAMES_FILE_PATH = "../data/names.txt"
MAP_TYPE = System::OBSTACLE_MAP # By deafult is obstacle map, 
								# If you want to change it as plain map, then change value as "System::PLAIN_MAP"
MOVE_COST = 300 				# You can change it whatever you want (must be positive integer)

simulator_name = "Simulator"
simulator_age = 18
simulator_id = nil
$system = nil

def add_number_of_driver(number_of_drivers)
	randomator = Random.new()
	grids = $system.get_map_grid()
	for i in 1 .. number_of_drivers do
		coords = Utilities.random_coordinate(0, 0, grids[0]-1, grids[1]-1)
		x, y = coords[0], coords[1]
		driver_age = Utilities.random_range(10, 60)
		name = Utilities.random_name_from_file(DEFAULT_NAMES_FILE_PATH)
		$system.add_user(name, "driver", driver_age, x, y)
	end
end

case ARGV[0].downcase() 			# this is case when the configuration is default
when "default"
	$system = System.new(20, 20, MOVE_COST)
	$system.generate_map(MAP_TYPE)
	coords = Utilities.random_coordinate(0, 0, 19, 19)
	simulator_id = $system.add_user(simulator_name, "customer", simulator_age, coords[0], coords[1])	
	add_number_of_driver(5)
when "advanced"						#this is case when grid length and user position is customable
	map_size = ARGV[1].to_i()
	user_x_position = ARGV[2].to_i()
	user_y_position = ARGV[3].to_i()
	$system = System.new(map_size, map_size, MOVE_COST)
	$system.generate_map(System::OBSTACLE_MAP)
	begin
		simulator_id = $system.add_user(simulator_name, "customer", simulator_age, user_x_position, user_y_position)
	rescue(WrongPositionError)
		abort("Oops, you enter customer's position wrongly, please enter location within the grid range")
	end
	add_number_of_driver(5)			
when "custom"						#this is case when you already have file as input
	lines = []
	loop do
		if ARGV.length() == 1
			path = "../data/input/input.txt"
		else
			path = ARGV[1]
		end
		begin
			file = File.open(path, "r")
			file.each_line do |line|
				lines += ["#{line}"]
			end
			done = true
		rescue
			puts("Oops, there is no a file from that path!")
			print("So, where is the file? ")
			path = STDIN.gets().chomp()
			done = false
		ensure
			file.close() unless file.nil?
		break if done
		end
	end

	map_size = lines[0].split()
	user_positions = lines[1].split()
	number_of_drivers = lines[2].to_i()
	lines.slice!(0 .. 2)
	driver_positions = lines.map() {|element| element.split()}

	$system = System.new(map_size[0].to_i, map_size[1].to_i, 300)
	$system.generate_map(MAP_TYPE)
	begin
		simulator_id = $system.add_user(simulator_name, "customer", simulator_age, 
			user_positions[0].to_i, user_positions[1].to_i)
		number_of_drivers.times() do |index|
			driver_age = Utilities.random_range(10, 60)
			name = Utilities.random_name_from_file(DEFAULT_NAMES_FILE_PATH)
			$system.add_user(name, "driver", driver_age, 
				driver_positions[index][0].to_i(), driver_positions[index][1].to_i())
		end
	rescue(WrongPositionError)
		abort("Oops, you enter user's position wrongly, please enter location within the grid range")
	end
end

def print_command_list()
	examples = ["go 6 2", "map 2", "exit", "map 1", "history"]
	puts("Command list: ",
	"(1) map [option]",
	"(2) go [x] [y]",
	"(3) history",
	"(4) exit",
	"Ex: \"#{examples.sample()}\" (Without double quotes)")
end

def print_notice()
	puts "Oops... you are commanding wrongly, sir",
		"if you forget the commands type \"help\" (Without double quotes)"
end
print "Launching app!"
puts

puts("Hello, customer! Let's make a command to me!")
print_command_list()
loop do
	choices = STDIN.gets().chomp().split()
	if choices[0].downcase() == "exit"
		puts "\nThank you for using this application, have a nice day!"
		break
	elsif choices[0].downcase() == "help"
		print_command_list()
	elsif choices[0].downcase() == "map"
		if choices.length() == 1
			print_notice()
		elsif choices[1].downcase() == "1"
			puts "Mapping..."
			puts $system.get_map(simulator_id)
			puts "Done!"
		elsif choices[1].downcase() == "2"
			puts "Mapping..."
			Utilities.create_file($system.get_map(simulator_id), DEFAULT_MAP_FILE_PATH, "w")
			puts "Done!"
		end
	elsif choices[0].downcase() == "go"
		if choices[1, 2].length == 2
			new_x, new_y = choices[1].to_i(), choices[2].to_i()
			information_id = nil
			begin
				information_id = $system.order_go_ride(simulator_id, new_x, new_y)
			rescue(NoPathSolutionError)
				puts "Sir, you can't choose that location, because that location is not path sign or user's symbol"
				next
			end
			map = $system.use_information(information_id[0])
			history = $system.get_information(information_id[1])
			Utilities.create_file(map, DEFAULT_MAP_FILE_PATH, "w")
			puts map
			puts history
			print "Confirmed the trip? (y/n)? "
			choice = STDIN.gets().chomp().downcase()
			if choice == "y"
				puts "processing... "
				puts $system.go_ride(simulator_id, information_id[1])
				puts "Done!"
			end
		else
			print_notice()
		end
	elsif choices[0].downcase() == "history"
		puts $system.get_history(simulator_id) 
	else
		print_notice()
	end
end