# Go-Cli Application

An application that can generate map whether with obstacles or not, and solve optinum path between two points randomly. Moreover, it can to be used as like transportation online application.

## Getting Started

First of all, install ruby and then it can run in terminal or cmd.

## Preparation

Now, open terminal and go to bin folder from this repository.
These are 3 commands you can use it.

````
ruby go-cli.rb default
```

```
ruby go-cli.rb advanced [grid] [x] [y]
```

```
ruby go-cli.rb custom [path] 
```

First one is default configuration that created 20 x 20 grid map, 5 random drivers, and 1 random customer.
So you can just type it like that:

```
ruby go-cli.rb default
```

Second is advanced configuration with custom grid of map and user position,
- [grid] = grid length
- [x] = customer's x position
- [y] = customer's y position
- The drivers will put in map randomly, and same as before it just 5 drivers are placed randomly.
Usage example:

```
ruby go-cli.rb advanced 28 13 9
```

The last one is file input, the file contains many lines and it has been formatted.
The first line, there are x grid length and y grid length.
The second line is user position, x and y.
The third line is number of drivers will be registered to the map, represented by N.
The next line and so on as much as N lines is driver position.
File example:

```
10 20
9 3
5
1 7
9 2
8 3
2 18
3 2
```
The path is relative, it depends on from go-cli.rb file
For example you can run like this:

```
ruby go-cli.rb custom ../data/input/input.txt
```

But don't worry, you still can run it without second argument, I means the path.
So it will like this:


```
ruby go-cli.rb custom
```

You just change the inner text from ..data/input/input.txt if you don't want to give a file.

## Running the App
When the app is running, then there are 4 commands.
Basically it just prints a map, if you change [option] as "1" then it will prints in terminal. And change [option] as "2", the app creates map in file. The file exists in data/output/map.txt
```
map [option]
```

This thing will moves customer to certain position, x and y are destination position.
```
go [x] [y]
```

It will tells your all trips from the beginning your trip until the last one. And of course it creates/overwrites file in "data/user history/[customer name].txt"
```
history
```

Just exit the app.
```
exit
```

## The Rule of App
- You can't go position in wall's position or beyond the map
- You can go anywhere you want, but just in path sign or user symbol

## The Things Should to Know
- The map always has route for every two points and never exist a blocked-route
- The map can generate obstacle map or plain map, just change in source code if you want to
- Every position can overlap another object, like there are user and driver in same location or many drivers in same location
- Wherever you go always there is a driver will give you a ride and then the driver will be placed anywhere in map randomly
- x grid means horizontal line, y grid means vertical line
- The format of customer's history is "[x before], [y before], [x after], [y after], [distance], [cost], [driver's name], [driver's position (x)], [driver's position (y)]" in one line
- The name of history file is customer's name and id, so it must be unique
- History file contains user history and it exists in path "[repo]/data/output/user history/[user id].txt
- Naming in program is case insensitive
- Whether driver or customer can be placed in range 0 <= x < [x grid length] and 0 <= y < [y grid length]
- for map larger than 40 x 40, I suggests you to use the map has been provided in data/output/map.txt and setting it as unwrapped text
- Default cost is 300/move, but you can change it in source code if you want to
- After the program exits then user history will keeps in that place, buat when you were running the application again, the inner file can be deleted
- Have fun!